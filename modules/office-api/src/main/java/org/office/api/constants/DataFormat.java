package org.office.api.constants;

public enum DataFormat {
	DATE("Date", "yyyy-MM-dd")
	,TIME("Time", "hh:mm:ss")
	,DATETIME("DateTime", "yyyy-MM-dd hh:mm:ss")
	;
	
	private DataFormat(String key_, String value_) {
		key = key_;
		value = value_;
	}
	
	private String key;
	private String value;
	
	public String key() {
		return key;
	}

	public void key(String key) {
		this.key = key;
	}

	public String value() {
		return value;
	}

	public void value(String value) {
		this.value = value;
	}
	
	public static DataFormat get(String key){
		for(DataFormat dataType : values()){
			if(dataType.key().equalsIgnoreCase(key)){
				return dataType;
			}
		}
		
		return DataFormat.DATE;
	}
	
}
