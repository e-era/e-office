package org.office.api.constants;

public enum DataType {
	
	DATE("Date", "日期")
	,TIME("Time", "时间")
	,DATETIME("DateTime", "日期时间")
	,BOOLEAN("Boolean", "布尔")
	,INTEGER("Integer", "整型")
	,LONG("Long", "长整型")
	,DOUBLE("Double", "浮点")
	,STRING("String", "字符")
	,FORMULA("Formula", "公式")
	;
	
	private DataType(String key_, String value_) {
		key = key_;
		value = value_;
	}
	
	private String key;
	private String value;
	
	public String key() {
		return key;
	}

	public void key(String key) {
		this.key = key;
	}

	public String value() {
		return value;
	}

	public void value(String value) {
		this.value = value;
	}
	
	public static DataType get(String key){
		for(DataType dataType : values()){
			if(dataType.key().equalsIgnoreCase(key)){
				return dataType;
			}
		}
		
		return DataType.STRING;
	}
	
}
