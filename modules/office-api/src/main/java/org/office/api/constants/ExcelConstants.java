package org.office.api.constants;

/** 
 * Excel常量池
 *
 * <pre> 
 * 构建组：office-api
 * 作者：eddy
 * 邮箱：1546077710@qq.com
 * 日期：2016年11月2日-下午2:15:05
 * 版权：
 * </pre>
 */
public class ExcelConstants {
	public static final String EXCEL_2003_POSTFIX = "xls";
	public static final String EXCEL_2007_POSTFIX = "xlsx";
	public static final String EMPTY = "";
	public static final String POINT = ".";
	public static final String NOT_EXCEL_FILE = " : Not the Excel file!";
	public static final String UNKNOW_FILE = " : unknow file!";
}
