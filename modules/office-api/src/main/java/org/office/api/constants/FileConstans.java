package org.office.api.constants;

public class FileConstans {
	public final static String EXCEL = "excel";
	public final static String WORD = "word";
	public final static String ETXT = "etxt";
	public final static String CSV = "csv";
}
