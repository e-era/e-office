package org.office.api.constants;

public class WordConstants {
	public static final String WORD_2003_POSTFIX = "doc";
	public static final String WORD_2007_POSTFIX = "docx";
	public static final String EMPTY = "";
	public static final String POINT = ".";
	public static final String NOT_WORD_FILE = " : Not the Word file!";
}
