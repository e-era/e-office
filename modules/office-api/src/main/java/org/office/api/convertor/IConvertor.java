package org.office.api.convertor;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.office.api.model.ModelClass;

/** 
 * 转换器接口
 *
 * <pre> 
 * 构建组：office-api
 * 作者：eddy
 * 邮箱：1546077710@qq.com
 * 日期：2016年11月2日-下午2:32:41
 * 版权：
 * </pre>
 */
public interface IConvertor {
	
	/**
	 * 获取文件类型
	 *
	 * @return 
	 */
	String type();
	
	/**
	 * 获取配置文件doc对象
	 *
	 * @return
	 * @throws Exception 
	 */
	Document configDoc(String confPath) throws Exception;
	
	/**
	 * 读取模型列表
	 *
	 * @return 
	 */
	List<ModelClass> readData(String fileName, InputStream stream, List<ModelClass> modelClassList)throws IOException;
	
	/**
	 * 读取模型列表
	 *
	 * @return 
	 */
	List<ModelClass> readData(String fileName, InputStream stream, ModelClass modelClass)throws IOException;
	
	/**
	 * 读取模型列表
	 *
	 * @return 
	 */
	List<ModelClass> readData(String fileName, String filePath, ModelClass modelClass)throws IOException;
	
	/**
	 * 获取模型列表数据
	 *
	 * @return 
	 */
	List<ModelClass> modelClassList(String confPath) throws Exception;
	
	/**
	 * 转换数据
	 *
	 * @return 
	 */
	List<ModelClass> convert(String confPath, String filePath) throws Exception;
	
	/**
	 * 转换数据
	 *
	 * @return 
	 */
	List<ModelClass> convert(String confPath, String fileName, InputStream stream) throws Exception;
	
	/**
	 * 生成sql
	 *
	 * @param modelClass
	 * @return 
	 */
	String convert2Sql(ModelClass modelClass);
	
	/**
	 * 生成sql参数
	 *
	 * @param modelClass
	 * @return 
	 */
	List<Map<String, Object>> convert2SqlParamValue(List<ModelClass> modelClass);
	
}
