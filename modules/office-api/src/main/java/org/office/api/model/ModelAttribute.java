package org.office.api.model;

/**
 * 模型属性实体
 *
 * <pre>
 *  
 * 构建组：office-api
 * 作者：eddy
 * 邮箱：1546077710@qq.com
 * 日期：2016年11月2日-下午2:29:15
 * 版权：
 * </pre>
 */
public class ModelAttribute implements Cloneable {
	private boolean pk;
	private boolean auto;
	private String name;
	private String field;
	private String value;
	private int index;
	private String bean;

	public boolean isPk() {
		return pk;
	}

	public void setPk(boolean pk) {
		this.pk = pk;
	}

	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}

	@Override
	public ModelAttribute clone() {
		ModelAttribute obj = null;
		try {
			obj = (ModelAttribute) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return obj;
	}

	@Override
	public String toString() {
		return "ModelAttribute [pk=" + pk + ",name=" + name + ", field=" + field + ", value=" + value + ", index=" + index + ", bean="
				+ bean + "]";
	}
	
}
