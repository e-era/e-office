package org.office.api.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 模型实体
 *
 * <pre>
 *  
 * 构建组：office-api
 * 作者：eddy
 * 邮箱：1546077710@qq.com
 * 日期：2016年11月2日-下午2:26:47
 * 版权：
 * </pre>
 */
public class ModelClass implements Cloneable {
	private String name;/*名称*/
	private String table;/*表名*/
	private String schemas;/*模式*/
	private int sn;/*序号*/
	private int index;/*索引*/
	private List<ModelAttribute> attributes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getSchemas() {
		return schemas;
	}

	public void setSchemas(String schemas) {
		this.schemas = schemas;
	}

	public int getSn() {
		return sn;
	}

	public void setSn(int sn) {
		this.sn = sn;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public List<ModelAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<ModelAttribute> attributes) {
		this.attributes = attributes;
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ModelClass clone() {
		try {
			ModelClass obj = (ModelClass) super.clone();
			List attrs = new ArrayList();
			for (Iterator iter = attributes.iterator(); iter.hasNext();) {
				ModelAttribute excelAttribute = (ModelAttribute) iter.next();
				attrs.add(excelAttribute.clone());
			}
			obj.setAttributes(attrs);
			return obj;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		return "ModelClass [name=" + name + ", table=" + table + ", schemas=" + schemas + ", sn=" + sn + ", index="
				+ index + ", attributes=" + attributes + "]";
	}
	
}
