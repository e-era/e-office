package org.office.api.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipOutputStream;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.types.FileSet;

/** 
 * 文件操作工具
 *
 * <pre> 
 * 构建组：office-api
 * 作者：eddy
 * 邮箱：1546077710@qq.com
 * 日期：2016年11月2日-下午4:53:24
 * 版权：
 * </pre>
 */
public class FileUtil {
	
	public static boolean isExist(String filePath){
		File f = new File(filePath);
		return f.exists();
	}
	
	public static boolean createDir(String filePath){
		File f = new File(filePath);
		return f.mkdir();
	}
	
	public static boolean createFile(String filePath){
		try {
			File f = new File(filePath);
			return f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static void setContent(String filePath, byte[] content){
		FileOutputStream stream = null;
		try {
			stream = new FileOutputStream(filePath);
			stream.write(content);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				stream.close();
			} catch (IOException ignore) {
			}
		}
	}
	
	public static int getIntFromFilePath(String filePath) {
		FileInputStream stream = null;
		try {
			File f = new File(filePath);
			if(!f.exists()){
				f.createNewFile();
				setIntFromFilePath(filePath, 0);
			}
			stream = new FileInputStream(f);
			return stream.read();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				stream.close();
			} catch (IOException ignore) {
			}
		}

		return 0;
	}
	
	public static void setIntFromFilePath(String filePath, int num) {
		FileOutputStream stream = null;
		try {
			File f = new File(filePath);
			if(!f.exists()){
				f.createNewFile();
			}
			stream = new FileOutputStream(f, false);
			stream.write(num);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				stream.close();
			} catch (IOException ignore) {
			}
		}
	}
	
	public static long getLongFromFilePath(String filePath) {
		FileInputStream stream = null;
		DataInputStream input = null;
		try {
			File f = new File(filePath);
			if(!f.exists()){
				f.createNewFile();
				setLongFromFilePath(filePath, 0L);
			}
			stream = new FileInputStream(f);
			input = new DataInputStream(stream);
			return input.readLong();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				stream.close();
				input.close();
			} catch (IOException ignore) {
			}
		}

		return 0;
	}
	
	public static void setLongFromFilePath(String filePath, long num) {
		FileOutputStream stream = null;
		DataOutputStream output = null;
		try {
			File f = new File(filePath);
			if(!f.exists()){
				f.createNewFile();
			}
			stream = new FileOutputStream(f, false);
			output = new DataOutputStream(stream);
			output.writeLong(num);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				stream.close();
				output.close();
			} catch (IOException ignore) {
			}
		}
	}
	
	public static byte[] getBytesFromFile(File f) {
		if (f == null) {
			return null;
		}

		try {
			FileInputStream stream = new FileInputStream(f);
			ByteArrayOutputStream out = new ByteArrayOutputStream(1000);

			byte[] b = new byte[1000];
			int n;
			while ((n = stream.read(b)) != -1) {
				out.write(b, 0, n);
			}
			stream.close();
			out.close();

			return out.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static byte[] getBytesFromFilePath(String filePath) {
		File f = new File(filePath);
		try {
			FileInputStream stream = new FileInputStream(f);
			ByteArrayOutputStream out = new ByteArrayOutputStream(1000);

			byte[] b = new byte[1000];
			int n;
			while ((n = stream.read(b)) != -1) {
				out.write(b, 0, n);
			}
			stream.close();
			out.close();
			return out.toByteArray();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String readLineForFilePath(String filePath) throws Exception {

		File f = new File(filePath);

		StringBuffer buf = new StringBuffer();
		BufferedReader br = null;
		try {
			InputStreamReader read = new InputStreamReader(new FileInputStream(
					f), "GBK");
			br = new BufferedReader(read);
			String line = null;
			line = br.readLine();
			if (line != null) {
				buf.append(line);
			}

		} catch (IOException ex) {
			throw new Exception("读文件" + filePath + "错误。", ex);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex1) {
			}
		}
		return buf.toString();
	}

	public static void writeForFilePath(String filePath, String txt)
			throws Exception {
		File f = new File(filePath);
		FileWriter out = null;
		try {
			out = new FileWriter(f);
			out.write(txt);
		} catch (IOException ex) {
			throw new Exception("写文件" + filePath + "错误。", ex);
		} finally {
			try {
				out.close();
			} catch (IOException ex1) {
			}
		}
	}

	public static void removeFile(File file, boolean isDirRemoveSelf) {
		if (file.exists()) {
			if (file.isDirectory()) {
				File[] list = file.listFiles();
				for (File tmp : list) {
					if (tmp.isDirectory()) {
						removeFile(tmp, true);
					} else {
						tmp.delete();
					}
				}
				if (isDirRemoveSelf) {
					file.delete();
				}
			} else {
				file.delete();
			}
		}
	}

	public static void removeFile(String file, boolean isDirRemoveSelf) {
		removeFile(new File(file), isDirRemoveSelf);
	}

	public static void zip(File file) throws Exception {
		try {
			if (file.exists()) {
				File parent = file.getParentFile();
				File zipFile = null;
				if(null == parent){
					parent = file;
				}
				zipFile = new File(parent.getAbsolutePath() + File.separator 
						+ file.getName() + "_" + System.currentTimeMillis() + ".zip");
				Project prj = new Project();    
				Zip zip = new Zip();    
				zip.setProject(prj);    
				zip.setDestFile(zipFile);    
				FileSet fileSet = new FileSet();    
				fileSet.setProject(prj);    
				if(file.isDirectory()){
					fileSet.setDir(file);
				}else{
					fileSet.setFile(file);
				}
				//fileSet.setIncludes("**/*.java"); //包括哪些文件或文件夹 eg:zip.setIncludes("*.java");    
				//fileSet.setExcludes(...); //排除哪些文件或文件夹    
				zip.addFileset(fileSet);    
				zip.execute();    
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public static void zip(String file) throws Exception {
		zip(new File(file));
	}
	
	public static void zip(File file, File zipFile) throws Exception {
		ZipOutputStream zout = null;
		FileInputStream fin = null;
		try {
			if (file.exists()) {
				Project prj = new Project();    
				Zip zip = new Zip();    
				zip.setProject(prj);    
				zip.setDestFile(zipFile);    
				FileSet fileSet = new FileSet();    
				fileSet.setProject(prj);    
				if(file.isDirectory()){
					fileSet.setDir(file);
				}else{
					fileSet.setFile(file);
				}
				//fileSet.setIncludes("**/*.java"); //包括哪些文件或文件夹 eg:zip.setIncludes("*.java");    
				//fileSet.setExcludes(...); //排除哪些文件或文件夹    
				zip.addFileset(fileSet);    
				zip.execute();    
			}
		} catch (Exception e) {
			throw e;
		}finally{
			if(null != zout)
				zout.close();
			if(null != fin)
				fin.close();
		}
	}

	public static void zip(String file, String zip) throws Exception {
		Pattern p1 = Pattern.compile("\\.zip$");
		Matcher m1 = p1.matcher(zip);
		if(!m1.matches()){
			zip += ".zip";
		}
		zip(new File(file), new File(zip));
	}
}
