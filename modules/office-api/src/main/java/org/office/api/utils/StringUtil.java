package org.office.api.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

/** 
 * 字符串操作工具
 *
 * <pre> 
 * 构建组：office-api
 * 作者：eddy
 * 邮箱：1546077710@qq.com
 * 日期：2016年11月2日-下午5:06:41
 * 版权：
 * </pre>
 */
public class StringUtil extends StringUtils{
	
	public static String toString(Object obj){
		if(null == obj){
			return "";
		}
		return obj.toString().trim();
	}
	
	private static Map<String, String> rome;
	static{
		rome = new HashMap<String, String>();
		rome.put("1", "I");
		rome.put("2", "II");
		rome.put("3", "III");
		rome.put("4", "IV");
		rome.put("5", "V");
		rome.put("6", "VI");
		rome.put("7", "VII");
		rome.put("8", "VIII");
		rome.put("9", "IX");
		rome.put("10", "X");
		rome.put("50", "L");
		rome.put("100", "C");
		rome.put("500", "D");
		rome.put("1000", "M");
	}
	
	public static String Arabic2Rome(String arabic){
		// 220kV1母，kV*母
//		Pattern pattern = Pattern.compile("");
//		Matcher matcher = pattern.matcher(arabic);
//		matcher.matches();
		StringBuilder result = new StringBuilder();
		int indexKV = arabic.indexOf("kV");
		indexKV = indexKV < 0 ? arabic.indexOf("KV") : indexKV;
		indexKV = indexKV < 0 ? arabic.indexOf("kv") : indexKV;
		int indexM = arabic.indexOf("母");
		if(indexKV > 0 && indexM > 0){
			String index = arabic.substring(indexKV + 2, indexM);
			if(null != rome.get(index)){
				result.append(arabic.substring(0, indexKV + 2))
					.append(rome.get(index))
					.append(arabic.substring(indexM));
			}else{
				result.append(arabic);
			}
		}else{
			result.append(arabic);
		}
		return result.toString();
	}
	
	public static String replace(String inString, String oldPattern,
			String newPattern) {
		if (inString == null)
			return null;
		if (oldPattern == null || newPattern == null)
			return inString;
		StringBuffer sbuf = new StringBuffer();
		int pos = 0;
		int index = inString.indexOf(oldPattern);
		int patLen = oldPattern.length();
		for (; index >= 0; index = inString.indexOf(oldPattern, pos)) {
			sbuf.append(inString.substring(pos, index));
			sbuf.append(newPattern);
			pos = index + patLen;
		}

		sbuf.append(inString.substring(pos));
		return sbuf.toString();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String[] tokenize(String source, String delimiters,
			boolean trimTokens, boolean ignoreEmptyTokens) {
		List tokens = new ArrayList();
		if (source == null) {
			return new String[0];
		}
		if (delimiters == null) {
			delimiters = ",";
		}
		StringTokenizer st = new StringTokenizer(source, delimiters);
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (trimTokens) {
				token = token.trim();
			}
			if ((!ignoreEmptyTokens) || (token.length() != 0)) {
				tokens.add(token);
			}
		}
		return (String[]) tokens.toArray(new String[tokens.size()]);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String[] tokenize1(String source, String delimiters,
			boolean trimTokens, boolean ignoreEmptyTokens) {
		List tokens = new ArrayList();
		if (source == null) {
			return new String[0];
		}
		if (delimiters == null) {
			delimiters = ",";
		}
		StringTokenizer st = new StringTokenizer(source, delimiters, true);
		int i = 0;
		String sPrevToken = "         ";
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.equals(delimiters)) {
				if (i == 0) {
					tokens.add("");
				}
				if (!st.hasMoreTokens()) {
					tokens.add("");
				}
				if (token.equals(sPrevToken)) {
					tokens.add("");
				}
			} else {
				if (trimTokens) {
					token = token.trim();
				}
				if ((!ignoreEmptyTokens) || (token.length() != 0)) {
					tokens.add(token);
				} else {
					tokens.add("");
				}
			}
			sPrevToken = token;
			i++;
		}
		return (String[]) tokens.toArray(new String[tokens.size()]);
	}
	
	public static String concat(String[] arr, int start, int end){
		StringBuilder builder = new StringBuilder();
		for(int i = start; i < end; i ++){
			builder.append(arr[i]);
		}
		return builder.toString();
	}
	
	public static boolean isEmpty(String value){
		if(null == value || "".equals(value))
			return true;
		return false;
	}
	
	public static boolean isBlank(String value){
		if(isEmpty(value)){
			return true;
		}else{
			for(int i = 0; i < value.length(); i ++){
				if(' ' != value.charAt(i)){
					return false;
				}
			}
		}
		return false;
	}
	
	public static String rate(double rate){
		rate = rate*100;
		return new java.text.DecimalFormat("#.00").format(rate) + "%";
	}
	
	public static String round(double rate, String format){
		rate = rate*100;
		return new java.text.DecimalFormat(format).format(rate);
	}
	
	public static String round(float rate, String format){
		rate = rate*100;
		return new java.text.DecimalFormat(format).format(rate);
	}
}
