package org.office.api.utils;

import org.office.api.constants.WordConstants;

public class WordUtil {
	
	
	public static String getPostfix(String path) {
		if (path == null || "".equals(path.trim())) {
			return "";
		}
		if (path.contains(WordConstants.POINT)) {
			return path.substring(path.lastIndexOf(WordConstants.POINT) + 1,
					path.length());
		}
		return "";
	}
}
