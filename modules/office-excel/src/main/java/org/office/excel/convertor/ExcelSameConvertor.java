package org.office.excel.convertor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.office.api.constants.ExcelConstants;
import org.office.api.constants.FileConstans;
import org.office.api.convertor.AbstractConvertor;
import org.office.api.model.ModelAttribute;
import org.office.api.model.ModelClass;
import org.office.api.utils.ExcelUtil;

/** 
 * <pre>
 * Excel转换器，所有sheet都是同一种数据类型，
 * 配置文件中的class节点只取第一个节点数据，
 * 读取所有存在sheet的数据，所以不能出现空的sheet，
 * 每一个sheet必须有数据且都要是相同的格式数据。
 * </pre>
 *
 * <pre> 
 * 构建组：office-excel
 * 作者：eddy
 * 邮箱：1546077710@qq.com
 * 日期：2016年11月2日-下午4:25:16
 * 版权：
 * </pre>
 */
public class ExcelSameConvertor extends AbstractConvertor {
	private Logger logger = Logger.getLogger(getClass());
	
	@Override
	public String type() {
		return FileConstans.EXCEL;
	}

	@Override
	public List<ModelClass> readData(String fileName, InputStream stream, List<ModelClass> modelClassList) throws IOException {
		if (null == fileName || "".equals(fileName)) {
			throw new IOException("文件名为空！");
		}
		
		if (null == stream) {
			throw new IOException("文件流为空！");
		}
		
		if (null == modelClassList || 0 == modelClassList.size()) {
			throw new IOException("模型对象列表为空！");
		}
		
		return readData(fileName, stream, modelClassList.get(0));
	}
	
	@Override
	public List<ModelClass> readData(String fileName, InputStream stream, ModelClass modelClass)throws IOException{
		if (null == fileName || "".equals(fileName)) {
			throw new IOException("文件名为空！");
		}
		
		if (null == stream) {
			throw new IOException("文件流为空！");
		}
		
		if (null == modelClass) {
			throw new IOException("模型对象为空！");
		}
		
		if (null == modelClass.getAttributes() || modelClass.getAttributes().size() < 1) {
			throw new IOException("模型对象属性列表为空！");
		}
		
		String postfix = ExcelUtil.getPostfix(fileName);
		if (ExcelConstants.EMPTY.equals(postfix)) {
			throw new IOException(fileName + ExcelConstants.NOT_EXCEL_FILE);
		}
		
		if (ExcelConstants.EXCEL_2003_POSTFIX.equals(postfix)) {
			POIFSFileSystem poifs = new POIFSFileSystem(stream);
			HSSFWorkbook hssfWorkbook = new HSSFWorkbook(poifs);
			HSSFSheet hssfSheet = null;

			// Read the Sheet
			List<ModelClass> rs = new ArrayList<ModelClass>();
			int sheets = hssfWorkbook.getNumberOfSheets();
			logger.debug("sheet numbers:" + sheets);
			for(int sheet = 0; sheet < sheets; sheet ++){
				logger.debug("read sheet number:" + sheet);
				hssfSheet = hssfWorkbook.getSheetAt(sheet);
				if(null == hssfSheet) continue;
				logger.debug("read sheet name:" + hssfSheet.getSheetName());
				
				List<ModelClass> tmpRs = readXls(hssfSheet, modelClass);
				if(null == tmpRs || tmpRs.size() < 1) continue;
				
				rs.addAll(tmpRs);
			}
			//hssfWorkbook.close(); //3.8-beta4没有这个方法
			return rs;
		} else if (ExcelConstants.EXCEL_2007_POSTFIX.equals(postfix)) {
			XSSFWorkbook xssfWorkbook = new XSSFWorkbook(stream);
			XSSFSheet xssfSheet = null;
			
			// Read the Sheet
			List<ModelClass> rs = new ArrayList<ModelClass>();
			int sheets = xssfWorkbook.getNumberOfSheets();
			logger.debug("sheet numbers:" + sheets);
			for(int sheet = 0; sheet < sheets; sheet ++){
				logger.debug("read sheet number:" + sheet);
				xssfSheet = xssfWorkbook.getSheetAt(sheet);
				if(null == xssfSheet) continue;
				logger.debug("read sheet name:" + xssfSheet.getSheetName());
				
				List<ModelClass> tmpRs = readXlsx(xssfSheet, modelClass);
				if(null == tmpRs || tmpRs.size() < 1) continue;
				
				rs.addAll(tmpRs);
			}
			//xssfWorkbook.close(); //3.8-beta4没有这个方法
			return rs;
		}else{
			throw new IOException(fileName + ExcelConstants.UNKNOW_FILE);
		}
	}
	
	@Override
	public List<ModelClass> readData(String fileName, String filePath, ModelClass modelClass)throws IOException{
		if(null == filePath || "".equals(filePath)) throw new IOException("文件路径为空！");
		
		InputStream stream = new FileInputStream(filePath);
		return readData(fileName, stream, modelClass);
	}
	
	/**
	 * 读取Excel 2003
	 *
	 * @param stream
	 * @param modelClassList
	 * @return
	 * @throws IOException 
	 */
	public static List<ModelClass> readXls(InputStream stream, List<ModelClass> modelClassList) throws IOException {
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(stream);
		List<ModelClass> results = new ArrayList<ModelClass>();
		HSSFSheet hssfSheet = null;
		
		for(ModelClass model : modelClassList){
			List<ModelAttribute> attributes = model.getAttributes();
			if(null == attributes || 0 == attributes.size()) {
				//hssfWorkbook.close(); //3.8-beta4没有这个方法
				throw new IOException("模型对象属性列表为空！");
			}
			
			// Read the Sheet
			int sheets = hssfWorkbook.getNumberOfSheets();
			for(int sheet = 0; sheet < sheets; sheet ++){
				hssfSheet = hssfWorkbook.getSheetAt(sheet);
				if(null == hssfSheet) if(null == hssfSheet) {
					//hssfWorkbook.close(); //3.8-beta4没有这个方法
					throw new IOException("工作簿" + (sheet + 1) + "不存在！");
				}
				
				List<ModelClass> tmpModelList = readXls(hssfSheet, model);
				if(null == tmpModelList || tmpModelList.size() < 1) continue;
				
				results.addAll(tmpModelList);
			}
		}
		
		//hssfWorkbook.close(); //3.8-beta4没有这个方法
		
		return results;
	}
	
	/**
	 * 读取Excel 2007
	 *
	 * @param stream
	 * @param modelClassList
	 * @return
	 * @throws IOException 
	 */
	public static List<ModelClass> readXlsx(InputStream stream, List<ModelClass> modelClassList) throws IOException {
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(stream);
		List<ModelClass> results = new ArrayList<ModelClass>();
		XSSFSheet xssfSheet = null;
		
		for(ModelClass model : modelClassList){
			List<ModelAttribute> attributes = model.getAttributes();
			if(null == attributes || 0 == attributes.size()) {
				//xssfWorkbook.close(); //3.8-beta4没有这个方法
				throw new IOException("模型对象属性列表为空！");
			}
			
			// Read the Sheet
			int sheets = xssfWorkbook.getNumberOfSheets();
			for(int sheet = 0; sheet < sheets; sheet ++){
				xssfSheet = xssfWorkbook.getSheetAt(sheet);
				if(null == xssfSheet) continue;
				
				List<ModelClass> tmpModelList = readXlsx(xssfSheet, model);
				if(null == tmpModelList || tmpModelList.size() < 1) continue;
				
				results.addAll(tmpModelList);
			}
		}
		
		//xssfWorkbook.close(); //3.8-beta4没有这个方法
		
		return results;
	}

}
