package org.office.excel.convertor;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.office.api.convertor.IConvertor;
import org.office.api.model.ModelAttribute;
import org.office.api.model.ModelClass;
import org.office.excel.convertor.ExcelDiffConvertor;
import org.office.excel.convertor.ExcelSameConvertor;

public class ExcelConvertorTest {
	Logger logger = Logger.getLogger(getClass());
	
	@Test
	public void testConvertString() {
		/*IConvertor convertor = new ExcelDiffConvertor();
		String confPath = "E:/security-workspace/x3/code/imports/modules/office-excel/src/test/resources/conf/model.xml";
		String excelPath = "E:/security-workspace/xl/各部门费用明细表.xls";
		try {
			Document doc = convertor.configDoc(confPath);
			List<ModelClass> confResults = convertor.modelClassList(confPath);
			logger.debug(confResults.size());
			for(ModelClass model : confResults){
				String sql = convertor.convert2Sql(model);
				logger.debug(sql);
				List<ModelClass> results = convertor.readData(excelPath, excelPath, model);
				logger.debug(results.size());
				List<Map<String, Object>> paramValue = convertor.convert2SqlParamValue(results);
				logger.debug(Arrays.toString(paramValue.toArray()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}
	
	@Test
	public void testFormula(){
		/*IConvertor convertor = new ExcelDiffConvertor();
		String confPath = "E:/security-workspace/x3/code/imports/modules/office-excel/src/test/resources/conf/model.xml";
		String excelPath = "E:/security-workspace/xl/工作/excel/测试数据/公式计算/结果数据.xlsx";
		try {
			List<ModelClass> confResults = convertor.modelClassList(confPath);
			logger.debug("配置sheet个数：" + confResults.size());
			for(ModelClass model : confResults){
				List<ModelClass> results = convertor.readData(excelPath, excelPath, model);
				logger.debug("数据条数：" + results.size());
				List<Map<String, Object>> paramValue = convertor.convert2SqlParamValue(results);
				logger.debug("数据内容：" + Arrays.toString(paramValue.toArray()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}
	
	@Test
	public void testFormulaRealSame(){
		IConvertor convertor = new ExcelSameConvertor();
		String confPath = "E:/security-workspace/x3/code/imports/modules/office-excel/src/test/resources/conf/modelf.xml";
		String excelPath = "E:/security-workspace/xl/工作/excel/测试数据/费用专线考核表-测试.xls";
		try {
			List<ModelClass> confResults = convertor.modelClassList(confPath);
			logger.debug("配置sheet个数：" + confResults.size());
			for(ModelClass model : confResults){
				List<ModelClass> results = convertor.readData(excelPath, excelPath, model);
				logger.debug("数据条数：" + results.size());
				//List<Map<String, Object>> paramValue = convertor.convert2SqlParamValue(results);
				//logger.debug("数据内容：" + Arrays.toString(paramValue.toArray()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFormulaRealDiff(){
		IConvertor convertor = new ExcelDiffConvertor();
		String confPath = "E:/security-workspace/x3/code/imports/modules/office-excel/src/test/resources/conf/modelf2.xml";
		String excelPath = "E:/security-workspace/xl/工作/excel/测试数据/费用专线考核表-测试.xls";
		try {
			List<ModelClass> confResults = convertor.modelClassList(confPath);
			logger.debug("配置sheet个数：" + confResults.size());
			for(ModelClass model : confResults){
				List<ModelClass> results = convertor.readData(excelPath, excelPath, model);
				logger.debug("数据条数：" + results.size());
				print(results);
				//List<Map<String, Object>> paramValue = convertor.convert2SqlParamValue(results);
				//logger.debug("数据内容：" + Arrays.toString(paramValue.toArray()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void print(List<ModelClass> results){
		StringBuilder builder = new StringBuilder("");
		for(ModelClass modelClass : results){
			builder.setLength(0);
			List<ModelAttribute> attrs = modelClass.getAttributes();
			builder.append(attrs.get(0).getName()).append("=").append(attrs.get(0).getValue()).append(",");
			builder.append(attrs.get(1).getName()).append("=").append(attrs.get(1).getValue()).append(",");
			builder.append(attrs.get(3).getName()).append("=").append(attrs.get(3).getValue());
			logger.debug(builder.toString());
		}
	}

}
